export type SiteConfigOptions = {
    title: string,
    subtitle: string
};

const config: SiteConfigOptions =  {
    title: "PRESSURE",
    subtitle: "Experimental Physically-Modelled Wind Instrument"
};

export default config;