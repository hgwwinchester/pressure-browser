import React, {useContext} from 'react';
import {ThemeState} from 'styles/types';
import Themes from 'styles/themes';

// TODO docs
export interface AppState extends ThemeState {
    // TODO Extend as need to
}

const initialState: AppState = {
    theme: Themes.DEFAULT,
    setTheme: () => {}
}

export const AppContext = React.createContext<AppState>(initialState);

export function useAppContext() {
    return useContext(AppContext);
}
