import {Cell} from "cfd/cell";
import {FaceInner, FaceOpen, FaceWall, IFace} from "cfd/face";
import {SuperFace} from "cfd/superface";
import {CFD, CFDOptions} from "cfd/simulate";
import {Theme} from "styles/types";
import {drawCell} from "cfd/CFDSketch";

// Create a flute mesh
export const createFluteMesh = (options: CFDOptions): [Cell[], IFace[]] => {
    const {length, cellW, cellH, cellD, airDensity} = options;
    const cellCount = Math.ceil(length/cellW);
    const area = cellH*cellD;
    const volume = area*cellW;
    // Tube A cells
    const cellsA: Cell[] = [];
    for (let i = 0; i < cellCount; i++) {
        cellsA.push(new Cell(cellW, volume, airDensity));
    }
    // Create tube faces
    const facesA: IFace[] = [];
    for (let i = 1; i < cellCount; i++) {
        facesA.push(new FaceInner(cellsA[i-1], cellsA[i], area, volume, 0));
    }
    facesA.push(new FaceOpen(cellsA[cellsA.length-1], +1, airDensity, area, volume/2, 0));
    // Tube B cells
    const cellCountB = Math.ceil(cellCount/4);
    const cellsB: Cell[] = [];
    for (let i = 0; i < cellCountB; i++) {
        cellsB.push(new Cell(cellW, volume, airDensity));
    }
    // Create tube faces
    const facesB: IFace[] = [];
    for (let i = 1; i < cellCountB; i++) {
        facesB.push(new FaceInner(cellsB[i-1], cellsB[i], area, volume, 0));
    }
    facesB.push(new FaceOpen(cellsB[cellsB.length-1], +1, airDensity, area, volume/2, 0));
    // Create flute mouthpiece
    const cells: Cell[] = [];
    const faces: IFace[] = [];
    const mouthCell = new Cell(cellW, volume, airDensity);
    const mouthTube = new Cell(cellW, volume, airDensity);
    cells.push(mouthCell);
    cells.push(mouthTube);
    faces.push(new FaceOpen(mouthCell, -1, airDensity, area, volume/2, 0));
    faces.push(new FaceInner(mouthCell, mouthTube, area, volume, 0));
    // Junction super-face
    const junction = new SuperFace(mouthTube, [cellsA[0], cellsB[0]], +1, area, volume/2, 0);
    faces.push(junction);
    // Add tubes
    cells.push(...cellsA);
    cells.push(...cellsB);
    faces.push(...facesA);
    faces.push(...facesB);
    // Update face volumes
    faces.forEach(f => f.updateVolume());
    return [cells, faces];
};

// Draw flute
export function drawFlute(p: any, cfd: CFD, scale: number, time: number, theme: Theme) {
    const [cells, faces, step, options] = cfd;
    const {cellW, cellH, airDensity} = options;
    const cellCount = Math.ceil(options.length/cellW);
    // Last two cells, and last three faces are for mouthpiece
    p.noStroke();
    cells.forEach((cell, i) => {
        if (i <= 1) return;
        const y = i-2 < cellCount ? 0.25 : 0.75
        const x = (i-2)%cellCount;
        drawCell(p, cell, (x+2)*(cell.length*scale), y*p.height, cellH, airDensity, scale, theme,  0.06);
    });
    const mouthCell = cells[0];
    const mouthTube = cells[0];
    drawCell(p, mouthCell, 0, 0.5*p.height, cellH, airDensity, scale, theme);
    drawCell(p, mouthTube, (cellW*scale), 0.5*p.height, cellH, airDensity, scale, theme);
    p.fill(theme.palette.info);
    p.textSize(32);
    p.textStyle(p.BOLD);
    p.textAlign(p.CENTER)
    p.text(`TIME = ${Math.floor(time*1000)}ms`, p.width/2, p.height-32);
}