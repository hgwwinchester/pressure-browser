import {AbstractVolume, IVolume} from "cfd/volume";
import {Cell} from "cfd/cell";

/**
 Faces are volumes half-way over two cells (or over a wall) with a mean flow velocity at
 the face/volume centroid.
 */

export interface IFace extends IVolume {
    initialArea: number,
    area: number,
    velocity: number,
    massFlux: number,
    advect: (dt: number) => void,
    step: (RT: number, dt: number) => void,
    update: (dt: number) => void,
    addVelocityFlux: (flux: number) => void
}

export abstract class AbstractFace extends AbstractVolume implements IFace {

    initialArea: number;
    area: number;
    massFlux: number;
    velocity: number;
    velocityFlux: number = 0;

    protected constructor(area: number, initialVolume: number, initialVelocity: number) {
        super(initialVolume);
        this.velocity = initialVelocity;
        this.area = area;
        this.initialArea = area;
        this.massFlux = 0;
        // Set volume flag to update
        this.raiseVolumeChanged();
    }

    addVelocityFlux(flux: number): void {
        this.velocityFlux += flux;
    }

    update(dt: number) {
        this.velocity += this.velocityFlux/this.volume;
        //if (Math.abs(this.velocity) > 343*0.1) {
        //    this.velocity = 343*0.1*Math.sign(this.velocity);
        //}
        this.velocityFlux = 0;
    }

    abstract advect(dt: number): void;
    abstract step(RT: number, dt: number): void;

}

// For the inner faces connecting two cells
export class FaceInner extends AbstractFace {

    protected cellL: Cell;
    protected cellR: Cell;

    constructor(cellL: Cell, cellR: Cell, area: number, initialVolume: number, initialVelocity: number, loop: boolean = false) {
        super(area, initialVolume, initialVelocity);
        this.cellL = cellL;
        this.cellR = cellR;
        this.cellL.assignRightFace(this);
        loop ? this.cellR.assignRightFace(this) : this.cellR.assignLeftFace(this);
    }

    advect(dt: number): void {
        // Update internal massFlux (let cells handle own updates)
        // Flux is given using the global "right pointing" direction, where +ve
        // indicates flow to the right, and -ve indicates flow the left
        // Mass flux shouldn't be confused with density flux - physical mass is advected,
        // not density
        //
        // Using Upwind Interpolation Scheme
        const p = this.velocity > 0 ? this.cellL.density : this.cellR.density;
        this.massFlux = p*this.velocity*this.area*dt;
    }

    step(RT: number, dt: number): void {
        // Calculate velocity deltas
        // du/dt = -convection + (1/p)(-pressureGradient + diffusion + viscosity)
        // Using Finite Volume Method
        // CONVECTION: Flow of velocity, using upwind scheme / uniform u
        const faceDest = this.velocity > 0 ? this.cellR.faceR : this.cellL.faceL;
        // @ts-ignore
        const area = 0.5*(this.area + faceDest.area);
        const conv = this.velocity*this.velocity*Math.sign(this.velocity)*area;
        // @ts-ignore
        faceDest.addVelocityFlux(conv*dt);
        // PRESSURE: Use pressure gradient
        const lengthT = this.cellL.length + this.cellR.length;
        const densityDifference = this.cellL.density - this.cellR.density;
        const pressure = this.volume*RT*(densityDifference/(0.5*lengthT));
        // Putting it all together...
        const density = -densityDifference*(this.cellL.length/lengthT) + this.cellL.density; // Lerp p
        const rhs = (1/density)*(pressure);
        this.addVelocityFlux((rhs - conv)*dt);
    }

    updateVolume() {
        if (!this.hasVolumeChanged()) return;
        this.volume = 0.5*(this.cellL.volume + this.cellR.volume);
        // TODO set flag for volume change to false
    }

}

// For walls (with only one cell connected)
export class FaceWall extends AbstractFace {

    protected cell: Cell;

    constructor(cell: Cell, normal: number, area: number, initialVolume: number, initialVelocity: number) {
        super(area, initialVolume, initialVelocity);
        this.cell = cell;
        normal > 0 ? this.cell.assignRightFace(this) : this.cell.assignLeftFace(this);
    }

    advect() {
        // Don't advect
    }

    step(dt: number) {
        // Velocity doesn't change
    }

    addVelocityFlux(flux: number): void {
        // Velocity doesn't change
    }

    update() {
        // Velocity doesn't change
    }

    updateVolume() {
        if (!this.hasVolumeChanged()) return;
        this.volume = 0.5*this.cell.volume;
    }

}

// Open face, exposed to some set density
export class FaceOpen extends AbstractFace {

    protected cell: Cell;
    protected normal: number;
    protected density: number;

    constructor(cell: Cell, normal: number, density: number, area: number, initialVolume: number, initialVelocity: number) {
        super(area, initialVolume, initialVelocity);
        this.cell = cell;
        this.normal = normal;
        this.density = density;
        normal > 0 ? this.cell.assignRightFace(this) : this.cell.assignLeftFace(this);
    }

    advect(dt: number) {
        const p = this.velocity*this.normal > 0 ? this.cell.density : this.density;
        this.massFlux = p*this.velocity*this.area*dt;
    }

    step(RT: number, dt: number) {
        let conv = this.velocity*this.velocity*Math.sign(this.velocity);
        // Doesn't convect out into open...
        if (this.velocity*this.normal < 0) {
            const faceDest = this.normal > 0 ? this.cell.faceL : this.cell.faceR;
            // @ts-ignore
            conv *= 0.5*(this.area + faceDest.area);
            // @ts-ignore
            faceDest.addVelocityFlux(conv*dt);
        }
        // Area out is effectively infinite, but that makes things hard, so just use face area with some multiplier
        // The multiplier affects loss (sustain)
        else conv *= this.area;
        // Assume doubling of cell length, for convenience
        const densityDifference = this.normal*(this.cell.density - this.density);
        const pressure = this.volume*RT*(densityDifference/this.cell.length);
        // Putting it all together...
        const density = 0.5*(this.cell.density + this.density); // Lerp p
        const rhs = (1/density)*(pressure);
        this.addVelocityFlux((rhs - conv)*dt);
    }

    updateVolume() {
        if (!this.hasVolumeChanged()) return;
        this.volume = this.cell.volume;
    }

}