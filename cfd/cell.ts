import {AbstractVolume} from "cfd/volume";
import {IFace} from "cfd/face";

/**
 Cells are the main "blocks" of volume in the model, and have an associated mean density.
 Cells have a left and right face, each with a velocity and mass flux.
 */
export class Cell extends AbstractVolume {

    faceL?: IFace;
    faceR?: IFace;
    length: number;
    density: number;

    constructor(length: number, initialVolume: number, initialDensity: number) {
        super(initialVolume);
        this.length = length;
        this.density = initialDensity;
    }

    assignLeftFace(face: IFace) {
        this.faceL = face;
    }

    assignRightFace(face: IFace) {
        this.faceR = face;
    }

    updateVolume(): void {
        if (!this.hasVolumeChanged()) return;
        const mass = this.density * this.volume;
        // Easier to not check...
        // @ts-ignore
        this.volume = 0.5*(this.faceL.area + this.faceR.area)*this.length;
        this.density = mass/this.volume;
    }

    update(): void {
        // @ts-ignore
        this.density += (this.faceL.massFlux - this.faceR.massFlux)/this.volume;
    }

}