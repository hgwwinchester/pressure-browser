import {CFD, CFDOptions, useCFD} from "cfd/simulate";
import {useAppContext} from "app/app-state";
import {Theme} from "styles/types";
import {IFace} from "cfd/face";
import {Cell} from "cfd/cell";
import {MutableRefObject, useRef} from "react";
import {useOnScreen} from "hooks/useOnScreen";
import ContainerP5 from "components/p5/ContainerP5";

type DrawHook = (time: number, cells: Cell[], faces: IFace[], options: CFDOptions) => void;

export function drawCell(p: any, cell: Cell, x: number, y: number, height: number, airDensity: number, scale: number, theme: Theme, interval: number = 0.05) {
    const t = p.map(cell.density, airDensity - interval, airDensity + interval, 0, 1);
    const h = p.map(cell.density, airDensity - interval, airDensity + interval, 0.1, 1.9);
    const c1 = p.lerpColor(p.color(theme.palette.info), p.color(theme.palette.main), p.map(t, 0, 0.5, 0, 1));
    const c2 = p.lerpColor(p.color(theme.palette.main), p.color(theme.palette.warning), p.map(t, 0.5, 1, 0, 1));
    const c = cell.density < 0 ? p.color(60) : t < 0.5 ? c1 : c2;
    p.fill(c);
    p.rect(x, y-(height*scale*h)*0.5, cell.length*scale+1, height*scale*h);
}

export function drawStandard(p: any, cfd: CFD, scale: number, time: number, theme: Theme, wave: number[] = []) {
    const [cells, faces, step, options] = cfd;
    const {cellW, cellH, airDensity} = options;
    p.noStroke();
    cells.forEach((cell, i) => {
        drawCell(p, cell, i*(cell.length*scale), p.height*0.25, cellH, airDensity, scale, theme);
    });
    const nuWave = wave.map((w, i) => i <= 0 ? (faces[faces.length-1].velocity)*1.25 : wave[i-1]);
    wave.forEach((w, i) => wave[i] = nuWave[i]);
    p.noFill();
    p.strokeWeight(3);
    p.stroke(p.color(theme.palette.info));
    p.beginShape();
    wave.forEach((w, i) => {
        if (i <= 0 || i >= wave.length - 1) p.vertex(i > 0 ? p.width : 0, p.height*0.75+w*5);
        else p.curveVertex(p.width-(p.width/wave.length)*i, p.height*0.75+w*5);
    });
    p.endShape();
    p.strokeWeight(1);
    p.line(0, p.height*0.75, p.width, p.height*0.75);
    p.noStroke();
    p.fill(theme.palette.info);
    p.textSize(32);
    p.textStyle(p.BOLD);
    p.textAlign(p.RIGHT);
    p.text(`${Math.floor(time*1000)}ms`, p.width-20, p.height-32);
    p.textAlign(p.LEFT)
    p.text(`${Math.floor((time-options.dt*wave.length)*1000)}ms`, 20, p.height-32)
    p.textAlign(p.CENTER)
    p.text("TIME, ms", p.width/2, p.height-32);
}

function p5Script(ref: MutableRefObject<null | HTMLElement>, onScreen: boolean, cfd: CFD, theme: Theme, hook?: DrawHook, aspect?: number) {
    const [cells, faces, step, options] = cfd;
    const {cellW, cellH} = options;
    let time = 0;

    const cellsX = options.cellsX ?? cells.length;
    const cellsY = options.cellsY ?? 1;
    const ratio = aspect ?? (cellsX*cellW)/(cellH*1.9*cellsY);

    const wave = new Array(512).fill(0);

    return (p: any) => {
        if (ref.current === null) return;

        // Handle scale
        let oW = ref.current?.clientWidth ?? 0;
        let oH = oW*(1/ratio)*3;
        let scale = Math.min(oW/(cellsX*cellW), oH/cellH);

        p.windowResized = () => {
            oW = ref.current?.clientWidth ?? 0;
            oH = oW*(1/ratio)*3;
            scale = Math.min(oW/(cellsX*cellW), oH/cellH);
            p.resizeCanvas(oW, oH);
        };

        p.setup = () => {
            p.createCanvas(oW, oH);
        };

        p.draw = () => {
            if (!onScreen) return;
            if (hook) hook(time, cells, faces, options);
            p.background(theme.palette.background);
            options.sketchFunction(p, cfd, scale, time, theme, wave);
            step();
            time += options.dt;
        };
    };
}

function CFDSketch(props: {hook?: DrawHook, aspect?: number, options: CFDOptions}) {
    const cfd: CFD = useCFD(props.options);
    const appContext = useAppContext();
    const ref = useRef(null);
    let onScreen = useOnScreen(ref, "-10px");
    return <div ref={ref} className={"container"}>
        <ContainerP5 script={p5Script(ref, onScreen, cfd, appContext.theme, props.hook, props.aspect)}/>
        <style jsx>{`
          .container {
            margin-top: ${appContext.theme.size.padding};
            margin-bottom: ${appContext.theme.size.padding};
            display: flex;
            justify-content: center;
          }
        `}</style>
    </div>
}

export default CFDSketch;