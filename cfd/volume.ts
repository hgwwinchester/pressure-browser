/**
 Volumes are classes with some 3D volume.
 Volume can change over time, but is updated lazily (using an update flag).
 */
export interface IVolume {
    raiseVolumeChanged: () => void,
    hasVolumeChanged: () => boolean,
    updateVolume: () => void
}

export abstract class AbstractVolume implements IVolume {

    volume: number;
    private volumeChanged: boolean = false;

    protected constructor(initialVolume: number) {
        this.volume = initialVolume;
    }

    raiseVolumeChanged(): void {
        this.volumeChanged = true;
    }

    hasVolumeChanged(): boolean {
        return this.volumeChanged;
    }

    abstract updateVolume(): void;

}