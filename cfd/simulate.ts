import {Cell} from "cfd/cell";
import {FaceInner, FaceOpen, FaceWall, IFace} from "cfd/face";
import {Theme} from "styles/types";

export const createMeshStandard = (options: CFDOptions): [Cell[], IFace[]] => {
    const {length, cellW, cellH, cellD, airDensity} = options;
    const cellCount = Math.ceil(length/cellW);
    const area = cellH*cellD;
    const volume = area*cellW;
    // Create cells
    const cells: Cell[] = [];
    for (let i = 0; i < cellCount; i++) {
        cells.push(new Cell(cellW, volume, airDensity));
    }
    // Create faces
    const faces: IFace[] = [];
    if (options.openLeft)
        faces.push(new FaceOpen(cells[0], -1, airDensity, area, volume/2, 0));
    else faces.push(new FaceWall(cells[0], -1, area, volume/2, 0));
    for (let i = 1; i < cellCount; i++) {
        faces.push(new FaceInner(cells[i-1], cells[i], area, volume, 0));
    }
    if (options.openRight)
        faces.push(new FaceOpen(cells[cells.length-1], +1, airDensity, area, volume/2, 0));
    else faces.push(new FaceWall(cells[cells.length-1], +1, area, volume/2, 0));
    // Update face volumes
    faces.forEach(f => f.updateVolume());
    return [cells, faces];
};

export type CFD = [
    cells: Cell[],
    faces: IFace[],
    step: () => void,
    options: CFDOptions
];

export type CFDOptions = {
    length: number,
    cellW: number,
    cellH: number,
    cellD: number,
    cellsX?: number,
    cellsY?: number,
    RT: number,
    dt: number,
    airDensity: number,
    openLeft: boolean,
    openRight: boolean,
    meshFunction: (options: CFDOptions) => [Cell[], IFace[]],
    sketchFunction: (p: any, cfd: CFD, scale: number, time: number, theme: Theme, wave: number[]) => void
};

export const useCFD = (options: CFDOptions): [Cell[], IFace[], () => void, CFDOptions] => {
    const {dt, RT} = options;
    const [cells, faces] = options.meshFunction(options);
    const step = () => {
        // 1. Update time step
        // 1.1 Advect
        for (const face of faces) face.advect(dt);
        for (const cell of cells) cell.update();
        // 1.2 Handle external forces/changes
        // TODO
        // 1.3 Correct volumes
        for (const cell of cells) cell.updateVolume();
        for (const face of faces) face.updateVolume();
        // 2. Update flow rates
        // 2.1 Step faces
        for (const face of faces) face.step(RT, dt);
        // 2.2 Update face velocities
        for (const face of faces) face.update(dt);
    };
    return [cells, faces, step, options]
}