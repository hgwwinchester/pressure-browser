import {CFD} from "cfd/simulate";
import {Theme} from "styles/types";
import {drawCell} from "cfd/CFDSketch";

export function drawReed(p: any, cfd: CFD, scale: number, time: number, theme: Theme, wave: number[] = []) {
    const [cells, faces, step, options] = cfd;
    const {cellW, cellH, airDensity} = options;
    p.noStroke();
    cells.forEach((cell, i) => {
        drawCell(p, cell, i*(cell.length*scale), p.height*0.25, cellH, airDensity, scale, theme);
    });
    const nuWave = wave.map((w, i) => i <= 0 ? (faces[faces.length-1].velocity)*1.25 : wave[i-1]);
    wave.forEach((w, i) => wave[i] = nuWave[i]);
    p.noFill();
    p.strokeWeight(3);
    p.stroke(p.color(theme.palette.info));
    p.beginShape();
    wave.forEach((w, i) => {
        if (i <= 0 || i >= wave.length - 1) p.vertex(i > 0 ? p.width : 0, p.height*0.5+w*5);
        else p.curveVertex(p.width-(p.width/wave.length)*i, p.height*0.5+w*5);
    });
    p.endShape();
    p.strokeWeight(1);
    p.line(0, p.height*0.5, p.width, p.height*0.5);
    p.noStroke();
    p.fill(theme.palette.info);
    p.textSize(32);
    p.textStyle(p.BOLD);
    p.textAlign(p.RIGHT);
    p.text(`${Math.floor(time*1000)}ms`, p.width-20, p.height-32);
    p.textAlign(p.LEFT)
    p.text(`${Math.floor((time-options.dt*wave.length)*1000)}ms`, 20, p.height-32)
    p.textAlign(p.CENTER)
    p.text("TIME, ms", p.width/2, p.height-32);
    p.noFill();
    p.strokeWeight(5);
    p.stroke(p.color(theme.palette.info));
    p.line(9*(options.cellW*scale), p.height*0.25-(options.cellH*scale)*((f)=>f.area/f.initialArea)(faces[9]), 19*(options.cellW*scale), p.height*0.25-(options.cellH*scale));
    p.line(9*(options.cellW*scale), p.height*0.25+(options.cellH*scale)*((f)=>f.area/f.initialArea)(faces[9]), 19*(options.cellW*scale), p.height*0.25+(options.cellH*scale));
}