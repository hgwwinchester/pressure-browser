import {AbstractFace} from "cfd/face";
import {Cell} from "cfd/cell";

/**
 * Super-faces and sub-faces are special face types.
 * They provide one-to-many connections by subdividing the area of a (super-) face into sub-faces.
 */

/**
 * Don't directly initialise...
 */
class SubFace extends AbstractFace {

    superFace: SuperFace;
    cell: Cell;
    protected normal: number;
    weight: number;
    weightVel: number = 0;
    presAcc: number = 0;

    constructor(superFace: SuperFace, cell: Cell, norm: number, w0: number, area: number, vol0: number, vel0: number) {
        super(area, vol0, vel0);
        this.superFace = superFace;
        this.normal = norm;
        this.weight = w0;
        this.cell = cell;
        this.normal > 0 ? this.cell.assignRightFace(this) : this.cell.assignLeftFace(this);
    }

    getOpposite() {
        return this.normal > 0 ? this.cell.faceL : this.cell.faceR;
    }

    advect(dt: number): void {
        const p = this.velocity*this.normal > 0 ? this.cell.density : this.superFace.cell.density;
        this.massFlux = p*this.velocity*this.area*dt;
    }

    update() {
        this.velocity += this.velocityFlux/this.volume;
        this.velocityFlux = 0;
    }

    step(RT: number, dt: number): void {
        // Handle pressure gradient
        // TODO handle lengths 2 w/ interpolation properly
        let dp = this.normal * (this.cell.density - this.superFace.cell.density) / (this.superFace.cell.length);
        const df = 0.5 * (this.cell.density + this.superFace.cell.density);
        dp *= (1 / df) * this.volume * RT * dt;
        this.addVelocityFlux(dp);
        this.presAcc = dp;

        const faceDest = this.velocity*this.normal > 0 ? this.superFace.getOpposite() : this.getOpposite();
        // Convect away to opposite cell
        // @ts-ignore
        const conv = Math.sign(this.velocity) * this.velocity * this.velocity * dt * 0.5*(this.area + faceDest.area);
        this.addVelocityFlux(-conv);
        this.getOpposite()?.addVelocityFlux(conv);
    }

    updateVolume(): void {
        if (!this.hasVolumeChanged()) return;
        this.volume = 0.5*(this.cell.volume + this.superFace.cell.volume);
    }

}

/**
 * Create a single super-face to handle linking all the sub-faces
 */
export class SuperFace extends AbstractFace {

    cell: Cell;
    subFaces: SubFace[];

    protected normal: number;
    protected w0: number;
    protected time: number = 0;

    constructor(one: Cell, many: Cell[], norm: number, area: number, vol0: number, vel0: number) {
        super(area, vol0, vel0);
        this.normal = norm;
        this.cell = one;
        this.w0 = 1/many.length;
        this.subFaces = many.map(cell => new SubFace(this, cell, -norm, this.w0, area, vol0, this.w0*vel0));
        this.normal > 0 ? this.cell.assignRightFace(this) : this.cell.assignLeftFace(this);
    }

    getOpposite() {
        return this.normal > 0 ? this.cell.faceL : this.cell.faceR;
    }

    advect(dt: number): void {
        this.subFaces.forEach(f => f.advect(dt));
        this.massFlux = this.subFaces.reduce((m, f) => m + f.massFlux, 0);
    }

    step(RT: number, dt: number): void {
        // Step sub-faces
        this.subFaces.forEach(f => f.step(RT, dt));
    }

    update(dt: number): void {
        // Get total v flux from sub-faces
        this.subFaces.forEach(f => f.addVelocityFlux(this.velocityFlux*f.weight));

        // Update sub-face weights
        const bal = this.subFaces.length - 1;
        const sw = this.subFaces.reduce((dw, f) => dw + f.presAcc, 0);
        const dw = this.subFaces.map(f => f.presAcc - (sw - f.presAcc)/bal);
        // this.subFaces.forEach((f, i) => f.addVelocityFlux(dw[i]*0.5));

        // Normalise weights...
        // const lw = Math.sqrt(w.reduce((l, w) => l + w*w, 0));
        // this.subFaces.forEach((f, i) => f.weight = w[i]);

        // Update sub-face velocity */
        // this.subFaces.forEach(f => f.velocity = f.weight*this.velocity);

        // Update sub-faces
        this.subFaces.forEach(f => f.update());
        this.velocityFlux = 0;
    }

    updateVolume(): void {
        this.subFaces.forEach(f => f.updateVolume());
        this.volume = this.subFaces.reduce((v, f) => v + f.volume, 0)/this.subFaces.length;
    }

    raiseVolumeChanged() {
        // TODO this doesn't account for sub-face changes well... Or maybe it does.
        // Give some more thought
        if (!this.subFaces) return;
        this.subFaces.forEach(f => f.raiseVolumeChanged());
    }

}