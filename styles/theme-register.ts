import {Theme, ThemeRegister} from 'styles/types';

// TODO docs
type Register = (name: string, theme: Theme, main?: boolean) => void;

// TODO docs
type Get = () => ThemeRegister;

// TODO docs
export default function themeRegister(): [Register, Get] {

    const tr: ThemeRegister = {};

    function register(name: string, theme: Theme, main = false): void {
        tr[name] = theme;
        if (main) tr['DEFAULT'] = theme;
    }

    function themes(): ThemeRegister {
        return {...tr};
    }

    return [register, themes];
}
