import themeRegister from 'styles/theme-register';

// TODO docs
const [register, get] = themeRegister();

// TODO docs
register('PRESSURE', {
    palette: {
        main: '#ff841f',
        active: '#ff841f',
        success: '#0bbd1d',
        warning: '#f00543',
        info: '#263bde',
        text: '#292929',
        background: '#F9FDFF'
    },
    size: {
        padding: "12px",
        fontRoot: "24px"
    },
    shadow: {
        main: "0 0 2px rgba(0, 0, 0, 0.2), 0 0 8px -2px rgba(0, 0, 0, 0.2)",
        mainGI: '0 4px 16px -4px #ff841f'
    },
    break: {
        small: 600,
        medium: 800
    }
}, true);

// TODO docs
const Themes = get();
export default Themes;
