// TODO docs
export interface ThemePalette {
    main: string
    active: string
    success: string
    warning: string
    info: string
    text: string
    background: string
}

export interface ThemeSizes {
    padding: string,
    fontRoot: string
}

export interface ThemeShadows {
    main: string,
    mainGI: string
}

export interface ThemeBreaks {
    small: number,
    medium: number
}

// TODO docs
export interface Theme {
    palette: ThemePalette,
    size: ThemeSizes,
    shadow: ThemeShadows,
    break: ThemeBreaks
}

export interface ThemeState {
    theme: Theme
    setTheme: () => void
}

export type ThemeRegister = Record<string, Theme>;
