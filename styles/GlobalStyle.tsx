import {useAppContext} from 'app/app-state';

// TODO docs
function GlobalStyle() {
    const appContext = useAppContext();
    return (
        <style jsx global>{`
            html {
              font-size: ${appContext.theme.size.fontRoot};
              font-family: 'Heebo', sans-serif;
            }
            body {
              margin: 0;
              color: ${appContext.theme.palette.text};
              background-color: ${appContext.theme.palette.background};
              text-align: justify;
            }
        `}</style>
    );
}

export default GlobalStyle;
