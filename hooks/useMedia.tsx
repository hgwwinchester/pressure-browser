import {useEffect, useState} from "react";

export enum MediaBreakpoint {
    MOBILE,
    TABLET,
    LAPTOP,
    DESKTOP
}

const queries: string[] = [
    '(max-width: 480px)',
    '(min-width: 481px) and (max-width: 768px)',
    '(min-width: 769px) and (max-width: 1024px)',
    '(min-width: 1025px)'
];

// Adapted from https://usehooks.com/useMedia/
export function useMedia() {
    const [value, setValue] = useState((): MediaBreakpoint => 0);

    useEffect(
        () => {
            const mediaQueryLists = queries.map((q) => window.matchMedia(q));

            // Gets value based on media query match
            const getValue = (): MediaBreakpoint => {
                // Get index of first media query that matches
                return mediaQueryLists.findIndex((mql) => mql.matches);
            };
            // Event listener callback
            // Note: By defining getValue outside of useEffect we ensure that it has ...
            // ... current values of hook args (as this hook callback is created once on mount).
            const handler = () => setValue(getValue);
            handler();
            // Set a listener for each media query with above handler as callback.
            mediaQueryLists.forEach((mql) => mql.addEventListener("change", handler));
            // Remove listeners on cleanup
            return () =>
                mediaQueryLists.forEach((mql) => mql.removeEventListener("change", handler));
        },
        [] // Empty array ensures effect is only run on mount and unmount
    );
    return value;

}