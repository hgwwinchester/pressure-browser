import {useAppContext} from "app/app-state";

// TODO docs
function FancyP(props: {children: string}) {
    const appContext = useAppContext();
    const text = props.children;
    const splitIndex = text.indexOf(" ", 12);
    const head = text.slice(0, splitIndex);
    const tail = text.slice(splitIndex);
    return <p>
        <span>{head}</span>
        {tail}
        <style jsx>{`
          span {
            color: ${appContext.theme.palette.main};
            font-weight: bold;
            text-shadow: ${appContext.theme.shadow.main};
            margin-right: 0.2rem;
          }
        `}</style>
    </p>
}

export default FancyP;