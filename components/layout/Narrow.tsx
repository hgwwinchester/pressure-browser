import {ReactNode} from "react";
import {useAppContext} from "app/app-state";

function Narrow(props: {children?: ReactNode}) {
    const appContext = useAppContext();
    return <div className={"container"}>
        <div className={"child"}>{props.children}</div>
        <style jsx>{`
          .container {
            display: flex;
            justify-content: center;
          }
          .child {
            max-width: 800px;
            margin: auto ${appContext.theme.size.padding};
          }
        `}</style>
    </div>
}

export default Narrow;