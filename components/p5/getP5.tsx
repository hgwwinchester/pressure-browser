import Script from "next/script";

let p5Global: any = null;
const listener: {callbacks: ((p: any)=>void)[], fire: () => void} = {
    callbacks: [],
    fire: () => {
        listener.callbacks.forEach(callback => callback(p5Global))
    }
};

export function ScriptP5() {
    return <Script
        src={"/scripts/p5.min.js"}
        onLoad={() => {
            // A little craziness to get p5 global
            // @ts-ignore
            p5Global = p5;
            console.log("p5.js has loaded...");
            listener.fire();
        }}
        defer
    />;
}

export function getP5(callback: (p: any) => void) {
    if (p5Global === null) listener.callbacks.push(callback);
    else callback(p5Global);
}