import {useRef, useEffect} from "react";
import {useAppContext} from "app/app-state";
import {ScriptP5, getP5} from "components/p5/getP5";

// TODO docs
function ContainerP5(props: {script: (p: any) => void, shadow?: boolean, border?: boolean}) {
    const rootRef = useRef(null);
    const appContext = useAppContext();
    useEffect(() => {
        let unmount = false;
        let pInstance: any = null;
        const wrapper = (p: any) => {
            pInstance = p;
            props.script(p);
        };
        getP5((p5: any) => {
            if (unmount) return;
            new p5(wrapper, rootRef.current === null ? undefined : rootRef.current);
        });
        return () => {
            unmount = true;
            if (pInstance !== null) pInstance.remove();
        };
    });
    return <div ref={rootRef}>
        <ScriptP5 />
        <style jsx>{`
          div {
            box-shadow: ${props.shadow ? appContext.theme.shadow.main : "none"};
            border: ${props.border ? `solid 4px ${appContext.theme.palette.main}` : "none"};
          }
        `}</style>
    </div>
}

export default ContainerP5;