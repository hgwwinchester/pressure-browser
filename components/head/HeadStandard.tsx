import Head from 'next/head';

type Props = {
    title: string
};

// TODO docs
function HeadStandard(props: Props) {
    return <Head>
        <title>{props.title}</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin={'use-credentials'} />
        <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@300;500;900&display=swap" rel="stylesheet" />
    </Head>
}

export default HeadStandard;