import {useAppContext} from 'app/app-state';

// TODO docs
function Title (props: {title: string, subtitle?: string, author?: string}) {
    const appContext = useAppContext();
    return <div className={"container"}>
        <div className={"border"}>
            <h1>{props.title.toLocaleUpperCase()}</h1>
            {props.subtitle ? <p className={"sub"}>{props.subtitle.toLocaleUpperCase()}</p> : <></>}
            {props.author ? <p className={"auth"}>By {props.author.toLocaleUpperCase()}</p> : <></>}
        </div>
        <style jsx>{`
          .container {
            color: ${appContext.theme.palette.main};
            display: flex;
            flex-direction: column;
            text-align: center;
            padding: ${appContext.theme.size.padding};
          }
          .border {
            align-self: center;
            max-width: 800px;
            width: 100%;
            border-bottom: solid 4px ${appContext.theme.palette.main};
            border-top: solid 4px ${appContext.theme.palette.main};
          }
          h1 {
            font-size: 2rem;
            text-align: center;
            margin: 0;
          }
          p {
            margin: 0;
          }
          .sub {font-weight: bold}
          .auth {
            font-weight: lighter;
            font-size: 0.8rem;
          }
        `}</style>
    </div>
}

export default Title;