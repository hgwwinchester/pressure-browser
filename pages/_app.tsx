import type { AppProps } from 'next/app';
import {Component} from 'react';
import Themes from 'styles/themes';
import GlobalStyle from 'styles/GlobalStyle';
import {AppState, AppContext} from 'app/app-state';

// TODO docs
class App extends Component<AppProps, AppState> {

    constructor(props: AppProps) {
        super(props);
        this.state = {
            theme: Themes.DEFAULT,
            setTheme: this.setTheme.bind(this)
        };
    }

    setTheme() {
        // TODO Whatever this is
    }

    render() {
        return (
            <AppContext.Provider value={this.state}>
                <GlobalStyle />
                <this.props.Component {...this.props.pageProps} />
            </AppContext.Provider>
        );
    }

}



export default App
