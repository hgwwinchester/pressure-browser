import {useAppContext} from "app/app-state";
import HeadStandard from "components/head/HeadStandard";
import config from "site-config";
import Title from "components/title/Title";
import CFDSketch, {drawStandard} from "cfd/CFDSketch";
import {Cell} from "cfd/cell";
import {IFace} from "cfd/face";
import {CFDOptions, createMeshStandard} from "cfd/simulate";
import Narrow from "components/layout/Narrow";
import FancyP from "components/layout/FancyP";
import {MutableRefObject, useRef, useState} from "react";
import Spacer from "components/layout/Spacer";
import {createFluteMesh, drawFlute} from "cfd/flute";
import {drawReed} from "cfd/reed";

const Time = (props: {setTimeRef: MutableRefObject<(time: number) => void>}) => {
    const appContext = useAppContext();
    const [time, setTime] = useState(0);
    props.setTimeRef.current = setTime;
    return <p>
        Elapsed time: <span>{Math.floor(time*1000)}ms</span>
        <style jsx>{`
          p {
            color: ${appContext.theme.palette.background};
            background-color: ${appContext.theme.palette.main};
            font-weight: bold;
            padding-left: 0.2rem;
            padding-right: 0.2rem;
            margin-right: 0.2rem;
            box-shadow: ${appContext.theme.shadow.main}, ${appContext.theme.shadow.mainGI};
          }
        `}</style>
    </p>
};

// TODO docs
function Home() {
    const appContext = useAppContext();
    const timeRef = useRef<(time: number) => void>(() => {});
    const xRef = useRef<(x: number) => void>(() => {});
    return (
        <>
            <HeadStandard title={config.title}/>
            <Title title={config.title} subtitle={config.subtitle} author={"Henry Winchester"}/>
            <Narrow>
                <FancyP>
                    Below is an example of a CFD (Computational Fluid Dynamics) simulation of air inside a 1m tube with
                    its left side closed, and its right open. The air at the far left is excited for a brief period
                    at the start of the simulation. Over time, the air begins resonating, then loses energy through
                    the open end.
                </FancyP>
                <p>
                    Note, the simulation is far from real-time, since the sound waves would move too fast to see
                    (the excitation is 440Hz!)
                </p>
            </Narrow>
            <Spacer/>
            <Narrow>
                <Time setTimeRef={timeRef}/>
            </Narrow>
            <CFDSketch options={{
                length: 1,
                cellW: 0.008, // Min wavelength
                cellH: 0.05,
                cellD: 0.05,
                dt: 1 / 44100,
                RT: 287.05 * 293.15, // R * T
                airDensity: 1.2,
                openLeft: false,
                openRight: true,
                meshFunction: createMeshStandard,
                sketchFunction: drawStandard
            }} hook={(time: number, cells: Cell[], faces: IFace[], options: CFDOptions) => {
                cells[4].density += time * 440 < 4 ? 0.02 * Math.sin(Math.PI * 2 * time * 440) : 0;
                timeRef.current(time);
            }}/>
            <Spacer/>
            <Narrow>
                <FancyP>
                    More examples (of entirely open tubes) demonstrate the handling of more intense initial conditions,
                    such as a high density peak...
                </FancyP>
            </Narrow>
            <Spacer/>
            <CFDSketch options={{
                length: 1,
                cellW: 0.008, // Min wavelength
                cellH: 0.05,
                cellD: 0.05,
                dt: 1 / 44100,
                RT: 287.05 * 293.15, // R * T
                airDensity: 1.2,
                openLeft: true,
                openRight: true,
                meshFunction: createMeshStandard,
                sketchFunction: drawStandard
            }} hook={(time: number, cells: Cell[], faces: IFace[], options: CFDOptions) => {
                if (time <= 0) cells[Math.floor(cells.length / 2)].density = options.airDensity * 1.2;
            }}/>
            <Spacer/>
            <Narrow>
                <p>
                    ... or a high velocity peak.
                </p>
            </Narrow>
            <Spacer/>
            <CFDSketch options={{
                length: 1,
                cellW: 0.008, // Min wavelength
                cellH: 0.05,
                cellD: 0.05,
                dt: 1 / 44100,
                RT: 287.05 * 293.15, // R * T
                airDensity: 1.2,
                openLeft: true,
                openRight: true,
                meshFunction: createMeshStandard,
                sketchFunction: drawStandard
            }} hook={(time: number, cells: Cell[], faces: IFace[], options: CFDOptions) => {
                if (time <= 0) faces[Math.floor(faces.length / 2)].velocity = 20;
            }}/>
            <Spacer/>
            <Narrow>
                <FancyP>
                    Using a concept called "super-faces" and "sub-faces" - where super-faces are collections of
                    balanced, one-sided sub-faces - it's possible to create junctions in the tube, while staying in
                    one dimension. The behaviour driving super/sub-faces isn't physically accurate
                    (in fact, the sub-faces overlap), but it does make the model more powerful.
                </FancyP>
                <p>
                    The simulation below shows a tube with a junction part-way through, with the aim of simulating
                    the lip of a flute.
                </p>
                <p>
                    /// Disregard all that - this is closer to a closed tube :|
                </p>
            </Narrow>
            <Spacer/>
            <CFDSketch options={{
                length: 1,
                cellW: 0.008, // Min wavelength
                cellH: 0.05,
                cellD: 0.05,
                dt: 1 / 44100,
                RT: 287.05 * 293.15, // R * T
                airDensity: 1.2,
                openLeft: true,
                openRight: true,
                meshFunction: createFluteMesh,
                sketchFunction: drawFlute,
                cellsX: Math.ceil(1/0.008)+2,
                cellsY: 2
            }} hook={(time: number, cells: Cell[], faces: IFace[], options: CFDOptions) => {
                faces[0].velocity = time * 440 < 1 ? 20 * Math.sin(Math.PI * 0.5 * time * 440) : 20;
                // cells[cells.length-1].density = 1.2;
            }}/>
            <CFDSketch options={{
                length: 1,
                cellW: 0.008, // Min wavelength
                cellH: 0.05,
                cellD: 0.05,
                dt: 1 / 44100,
                RT: 287.05 * 293.15, // R * T
                airDensity: 1.2,
                openLeft: true,
                openRight: true,
                meshFunction: createMeshStandard,
                sketchFunction: drawReed
            }} hook={(time: number, cells: Cell[], faces: IFace[], options: CFDOptions) => {
                const startCell = 9;
                const D = 0.08;
                const m = 0.00001;
                const dt = 1 / 44100;
                const RT = 287.05 * 293.15;
                let dh = reedState.x;
                let A = Math.sqrt(dh*dh+0.01*D*D)*0.05;
                let k = 100;
                let p = - D*A*cells.slice(startCell, startCell+10).reduce((a, c, i) => {
                    return a + c.density*(0.1*(9-i))*RT*A;
                }, 0);
                const f = -k*(reedState.x-reedState.eq) + p - reedState.v*0.0001;
                let a = f/m;
                reedState.v += a*dt;
                reedState.x += reedState.v*dt;
                reedState.x = reedState.x > reedState.max ? reedState.max : reedState.x < reedState.min ? reedState.min : reedState.x;
                reedState.v = reedState.x >= reedState.max || reedState.x <= reedState.min ? 0 : reedState.v;
                // Update areas
                faces.slice(startCell, startCell+10).forEach((f, i) => {
                    f.area = f.initialArea*(1 - 2*(0.1*(10-i))*reedState.x);
                });
                // Osc
                faces[0].velocity = time * 440 < 1 ? 4 * Math.sin(Math.PI * 0.5 * time * 440) : 4;
            }}/>
            <Time setTimeRef={xRef}/>
        </>
    );
}

const reedState = {
    max: 0.5,
    min: 0,
    x: 0.5,
    v: 0,
    eq: 0.5
}

export async function getStaticProps() {
    return {
        props: {}
    }
}

export default Home